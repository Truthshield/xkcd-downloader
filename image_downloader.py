import requests
import os


def image_downloader(url, filename, ops, savepath=""):
    if savepath == "":  # Checking if savepath is empty
        savepath = os.getcwd()
    slash = "\\" if ops == "nt" else "/"  # Chooses slash based on an operating system
    r = requests.get(url)  # Downloading image/file
    with open(savepath + slash + filename + ".jpg", "wb") as f:  # Saving image
        f.write(r.content)
# image_downloader("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fi.ytimg.com%2Fvi%2FypG3yBxgDEs%2Fmaxresdefault.jpg&f=1&nofb=1", "bruh", "nt", r"C:\Users\Truthshield\Downloads\Compressed\XKCDownloader")
