import requests
import re
from bs4 import BeautifulSoup
from pathlib import Path


def number_of_comics() -> int:
    response = requests.get(r'https://ipfs.io/ipns/xkcd.hacdias.com/')
    lines = response.text.split('\n')
    for line in lines:
        if ">Latest</a>" in line:
            for i in line.split():
                if "href" in i:
                    return int(re.search(r'\d+', i).group(0))


def insert_zero(num) -> str:
    if num < 10:
        return '000' + str(num)
    elif num < 100:
        return '00' + str(num)
    elif num < 1000:
        return '0' + str(num)
    elif num < 10000:
        return '' + str(num)


def download_comic(comic_start, comic_end) -> None:
    url = r'https://ipfs.io/ipns/xkcd.hacdias.com/'
    dl_amount = comic_end - comic_start + 1
    for i in range(1, dl_amount + 1):
        current_comic = comic_start - 1 + i
        response = requests.get(url + insert_zero(current_comic) + '/')
        if response.status_code == 404:
            print('[' + str(i) + '/' + str(dl_amount) + '] Comic #' + str(current_comic) + " doesn't exist.")
        soup = BeautifulSoup(response.text, 'html.parser')
        for link in soup.find_all('img'):
            link = link.get('src')

            filename = str(link).replace('./', '')
            image_url = url + insert_zero(current_comic) + '/' + filename

            save_path = Path(Path.cwd(), 'xkcd', insert_zero(current_comic))
            if not Path.exists(save_path):
                Path.mkdir(save_path, parents=True)
            if not Path.is_file(Path(save_path, filename)):
                img = requests.get(image_url)
                with open(Path(save_path, filename), 'wb') as handle:
                    handle.write(img.content)
                print('[' + str(i) + '/' + str(dl_amount) + '] "' + str(Path(save_path, filename)) + '" downloaded.')
            else:
                print(
                    '[' + str(i) + '/' + str(dl_amount) + '] "' + str(Path(save_path, filename)) + '" already exists.')
