import url_extractor

close = False
options = ['exit program', 'print the number of comics', 'download comic(s)']

while not close:
    for i in range(0, len(options)):
        print(f'{i} - ' + options[i])

    chosen = input('Choose one of the options above: ')
    if not chosen.isnumeric():
        print('Invalid value.')
        continue

    chosen = int(chosen)
    if chosen == 0:
        close = True
    elif chosen == 1:
        print('Current number of comics: ' + str(url_extractor.number_of_comics()))
    elif chosen == 2:
        while True:
            comic_start = input('Enter a comic number to start with: ')
            if comic_start.isnumeric():
                comic_start = int(comic_start)
                if url_extractor.number_of_comics() < comic_start or comic_start < 1:
                    print('Invalid value.')
                    continue
                while True:
                    comic_end = input('Enter a comic number to end with: ')
                    if comic_end.isnumeric():
                        comic_end = int(comic_end)
                        if url_extractor.number_of_comics() < comic_end or comic_end < 1:
                            print('Invalid value.')
                            continue
                        else:
                            url_extractor.download_comic(comic_start, comic_end)
                            break
                if comic_end < comic_start:
                    print(f'Invalid value. {comic_end} is smaller than {comic_start}.')
                    continue
                break
            else:
                print('Invalid value.')
    else:
        print('Invalid value.')
